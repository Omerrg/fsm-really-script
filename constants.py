import os

FILE_NAME = "accounts.txt"
FILE_PATH = os.path.join(os.getcwd(), FILE_NAME)

ACCOUNT_TO_FOLLOW = "youtubetizziem"

DRIVER_PATH = os.path.join(os.getcwd(), "drivers", "chromedriver.exe")
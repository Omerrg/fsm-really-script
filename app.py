import constants
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep


def get_accounts_from_file():
    accounts = []
    with open(constants.FILE_PATH, 'r') as accounts_file:
        for line in accounts_file.readlines():
            accounts.append(line.split(':'))
    return accounts


def main():
    accounts = get_accounts_from_file()
    for username, password in accounts:
        driver = webdriver.Chrome(executable_path=constants.DRIVER_PATH)
        driver.get("http://www.instagram.com/accounts/login/?source=auth_switcher")
        username_element = driver.find_element_by_name("username")
        username_element.send_keys(username)
        password_element = driver.find_element_by_name("password")
        password_element.send_keys(password.replace("\n", ""))
        button_login = driver.find_element_by_class_name('L3NKy')
        button_login.click()
        sleep(3)
        notnow = None
        try:
            notnow = driver.find_element_by_class_name("HoLwm")
        except:
            print("Security code needed.. please respond.")
            input()
            notnow = driver.find_element_by_class_name("HoLwm")
            continue
        notnow.click()
        driver.get("https://www.instagram.com/{user_name_to_follow}/".format(user_name_to_follow=constants.ACCOUNT_TO_FOLLOW))
        try:
            follow_button = driver.find_element_by_class_name("_6VtSN")
            if follow_button.text == "Following":
                raise
            follow_button.click()
            print("Successfully followed using username: {user_name}.".format(user_name=constants.ACCOUNT_TO_FOLLOW))
        except:
            print("Account is already following {user_name}".format(user_name=constants.ACCOUNT_TO_FOLLOW))
        finally:
            driver.close()



if __name__ == '__main__':
    main()
